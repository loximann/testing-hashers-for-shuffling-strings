#include <algorithm>
#include <chrono>
#include <cstdint>
#include <functional>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <random>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

using hash_t = uint64_t;
using hasher_t = std::function<hash_t(const std::string&)>;

using seed_t = uint64_t;
using hasher_gen_t = std::function<hasher_t(const seed_t&)>;

using index_t = int;

std::vector<index_t> shuffle( const std::vector<std::string>& names,
                            const hasher_t& hasher ) {
        std::vector<std::pair<hash_t, index_t>> hashes;

        for(index_t index=0;index<names.size();++index)
        {
            hashes.push_back({hasher(names[index]), index});
        }

        std::ranges::sort(hashes.begin(), hashes.end(), []( const auto& left, const
        auto& right ) {
                return std::get<0>( left ) < std::get<0>( right );
                        });

            std::vector<index_t> result;
            std::transform( hashes.begin(),
                            hashes.end(),
                            std::back_inserter( result ),
                            []( const auto& x ) { return std::get<1>( x ); }
                            );

            return result;
}

bool compare(const std::vector<index_t>& left, const std::vector<index_t>& right) {
    return (left.size() == right.size()) && std::equal(left.begin(), left.end(), right.begin());
}

/****** Sample processing functions *******/
template <typename T>
std::map<T, int> count(const std::vector<T>& samples) {
    const std::set<T> unique_samples{samples.begin(), samples.end()};

    std::map<T, int> result;

    for(const auto& unique_sample: unique_samples) {
        result.emplace(unique_sample, std::count(samples.begin(), samples.end(), unique_sample));
    }
    return result;
}

template <typename T>
std::array<double, 2> statistics(const std::vector<T>& samples) {
    double x = 0.0;
    double xx = 0.0;
    const auto n = samples.size();
    for(const auto& sample: samples) {
        x += sample;
        xx += sample*sample;
    }
    return { x / n, sqrt( xx - x * x / n ) / n };
}

/****** Test runner *******/
// Run :: hasher -> sample
template<typename Run>
struct run_result {
    using type = std::invoke_result_t<Run, hasher_t>;
};

template<typename Run>
using run_result_t = run_result<Run>::type;

// Process :: [sample] -> result
template<typename Process, typename Sample>
struct process_result {
    using type = std::invoke_result_t<Process, const std::vector<Sample>&>;
};

template<typename Process, typename Sample>
using process_result_t = process_result<Process, Sample>::type;

template <typename Run, typename Process>
std::map<std::string, process_result_t<Process, run_result_t<Run>>>
run_many_seeds( const Run& sample,
                const Process& process,
                const std::map<std::string, hasher_gen_t> algorithms ) {
    std::random_device seed_generator;
    std::map<std::string, process_result_t<Process, run_result_t<Run>>> results;
    for ( const auto& kv : algorithms ) {
        const auto& name = kv.first;
        const auto& algorithm = kv.second;

        std::vector<run_result_t<Run>> samples;
        const int nsamples = 100000;
        for ( int i = 0; i < nsamples; i++ ) {
            const auto seed = seed_generator();
            samples.push_back( sample( algorithm( seed ) ) );
        }

        results.emplace(name, process( samples ));
    }
    return results;
}

namespace std {
    template <typename T, typename S>
    std::ostream& operator<<( std::ostream& os,
                              const std::pair<T, S>& pair ) {
        os << pair.second << " : " << pair.first;
        return os;
    }
    template <typename T, typename S>
    std::ostream& operator<<( std::ostream& os,
                              const std::map<T, S>& m ) {
        os << "{ ";
        for(auto iter = m.begin(); iter != m.end(); ++iter) {
            if ( iter != m.begin() ) {
                os << ", ";
            }
            os << "\"" << iter->first << "\" : " << iter->second;
        }
        os << "}";
        return os;
    }
    template <typename T>
    std::ostream& operator<<( std::ostream& os,
                              const std::vector<T>& v ) {
        os << "[ ";
        for(auto iter = v.begin(); iter < v.end(); ++iter) {
            if ( iter > v.begin() ) {
                os << ", ";
            }
            os << *iter;
        }
        os << "]";
        return os;
    }
    template <>
    std::ostream& operator<<( std::ostream& os,
                              const std::vector<index_t>& v ) {
        for (const auto i: v) {
            os << i;
        }
        return os;
    }
    template <typename T>
    std::ostream& operator<<( std::ostream& os,
                              const std::array<T, 2>& a ) {
        os << "[ " << a[0] << ", " << a[1] << "]";
        return os;
    }
} // namespace std

int main(int argc, char *argv[])
{
    hasher_gen_t fnv1a_mod{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            hash_t hash = seed;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            return hash;
        };
    } };

    hasher_gen_t fnv1a_mod_tail{ [&fnv1a_mod]( hash_t seed ) {
        return [hash = fnv1a_mod(seed)]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            auto result = hash(name);
            for ( const char c : "tail" ) {
                result ^= c;
                result *= prime;
            }
            return result;
        };
    }};

    hasher_gen_t fnv1a_mod_mult_fold{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            hash_t hash = seed;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            const auto low{static_cast<uint32_t>(hash)};
            const auto high{static_cast<uint32_t>(hash >> 32)};
            return hash_t{low*high};
        };
    } };

    hasher_gen_t fnv1a_mod_xor_fold{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            hash_t hash = seed;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            const auto low{static_cast<uint32_t>(hash)};
            const auto high{static_cast<uint32_t>(hash >> 32)};
            return hash_t{low^high};
        };
    } };

    hasher_gen_t fnv1a_new{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            hash_t hash = 14695981039346656037u;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            hash ^= seed;
            hash *= prime;
            return hash;
        };
    } };

    hasher_gen_t fnv1a_32{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const uint32_t prime = 16777619;
            uint32_t hash = 2166136261;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            hash ^= static_cast<uint32_t>(seed);
            hash *= prime;
            return hash;
        };
    } };

    hasher_gen_t fnv1a_new_mult_fold{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            hash_t hash = 14695981039346656037u;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            hash ^= seed;
            hash *= prime;
            const auto low{static_cast<uint32_t>(hash)};
            const auto high{static_cast<uint32_t>(hash >> 32)};
            return hash_t{low*high};
        };
    } };

    hasher_gen_t fnv1a_new_xor_fold{ []( hash_t seed ) {
        return [seed]( const std::string& name ) {
            const hash_t prime = 1099511628211u;
            hash_t hash = 14695981039346656037u;
            for ( const char c : name ) {
                hash ^= c;
                hash *= prime;
            }
            hash ^= seed;
            hash *= prime;
            const auto low{static_cast<uint32_t>(hash)};
            const auto high{static_cast<uint32_t>(hash >> 32)};
            return hash_t{low^high};
        };
    } };

    hasher_gen_t random{ []( hash_t seed ) {
        return [engine = std::default_random_engine{ seed }](
                   const std::string& /* ignore */ ) mutable { return engine(); };
    } };

    const std::map<std::string, hasher_gen_t> algorithms{{
        {"fnv1a_mod",           fnv1a_mod},
        {"fnv1a_mod_tail",      fnv1a_mod_tail},
        {"fnv1a_mod_xor_fold",  fnv1a_mod_xor_fold},
        {"fnv1a_mod_mult_fold", fnv1a_mod_mult_fold},
        {"fnv1a_new",           fnv1a_new},
        {"fnv1a_new_xor_fold",  fnv1a_new_xor_fold},
        {"fnv1a_new_mult_fold", fnv1a_new_mult_fold},
        {"random",              random}
    }};

//    for(const auto& n: {0l, 100000000l, 200000000l}) {
//        std::cout << fnv1a_new(1)(std::string(n, 'a')) << std::endl;
//    }
//    exit(1);
    std::map<std::string, std::string> results;
    /* Test 1: How homogeneous are the distributions of the sorts? */
{
    const auto shuffle_names{ []( const std::vector<std::string>& names ) {
        return [names]( const hasher_t& hasher ) {
            return shuffle( names, hasher );
        };
    } };
    const auto run_histograms{
        [&shuffle_names, &algorithms]( const std::vector<std::string>& names ) {
            const auto sample = shuffle_names( names );
            return run_many_seeds(
                sample, count<run_result_t<decltype( sample )>>, algorithms );
        } };
    for ( const auto& test : std::map<std::string, std::vector<std::string>>{
              { "different",
                { "Some name", "Another test", "Feature", "Foobar" } },
              { "two similar", { "a1", "a2", "Test test", "Unit tests" } },
              { "two similar pairs", { "a1", "a2", "b1", "b2" } },
              { "four similar", { "a1", "a2", "a3", "a4" } } } ) {
        const auto& label = test.first;
        const auto& names = test.second;
        std::ostringstream os;
        os << run_histograms( names );
        results.emplace( "Histograms " + label, os.str() );
    }
}

        /* Test 2: Speed */
{
    const auto time_one_hash{ [&algorithms]( const std::string& name ) {
        std::chrono::system_clock clock;
        hash_t dummy;
        const auto sample = [&clock, &dummy, &name]( const hasher_t& hasher ) {
            const auto t0 = clock.now();
            dummy += hasher(name);
            const auto t1 = clock.now();
            return std::chrono::duration<double>( t1 - t0 ).count();
        };
        return run_many_seeds(
            sample, statistics<run_result_t<decltype( sample )>>, algorithms );
    } };
    for ( const auto& kv : std::map<std::string, std::string>{
            {"empty", ""},
            {"40 char",std::string(40, 'a')},
            {"80 char",std::string(80, 'a')}
            } ) {
        const auto& label = kv.first;
        const auto& name = kv.second;
        auto timings = time_one_hash( name );
        std::ostringstream os;
        os << timings;
        results.emplace( std::string{"Timings "} + label, os.str() );
    }
}
    std::cout << results << std::endl;

    return 0;
}

