#!/usr/bin/env python3

import matplotlib.pyplot as plt
import json
import numpy as np

def normalize(x):
    as_array = np.array(x)
    return as_array/sum(as_array)

with open("result.out") as f:
    results = json.load(f)

for name, result in {k:v for k,v in results.items() if "Histogram" in k}.items():
    keys=sorted(result["random"].keys())
    for algorithm, histogram in result.items():
        plt.plot(keys, normalize([histogram.get(key, 0) for key in keys]), '-x', label=algorithm)

    plt.legend()
    plt.xticks(rotation=45)
    plt.xlabel("Permutation")
    plt.title(name)

    plt.ylim(bottom=0, top=0.3)
    plt.ylabel("Probability")

    plt.subplots_adjust(left=0.15, right=0.85, bottom=0.15, top=0.85)

    filename_root = name.replace(" ", "_")

    plt.savefig(filename_root+".jpg")

    plt.ylim(bottom=1/24*(1-1.e-1), top=1/24*(1+1.e-1))
    plt.savefig(filename_root+"_zoom.jpg")

    plt.cla()


timings = [f"Timings {t}" for t in ["empty", "40 char", "80 char"]]
print(20*" ", end="")
for t in timings:
    print(f"    {t:^19s}", end="")
print()

for algorithm in results["Timings empty"].keys():
    print(f"{algorithm:20s}", end="")
    for timing in [results[k][algorithm] for k in timings]:
        print(f"{timing[0]:12.2e}+/-{timing[1]:8.2e}", end="")
    print()

